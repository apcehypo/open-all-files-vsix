﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace OpenAllFilesVSIX
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class OpenSiblingFilesCommand : CommandBase
    {
        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("c227e1f2-bc2f-4dad-8f00-dccabd35c330");

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenSiblingFilesCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private OpenSiblingFilesCommand(Package package) : base(package)
        {
            this.dte2 = this.ServiceProvider.GetService(typeof(SDTE)) as DTE2;

            var commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new OleMenuCommand(this.MenuItem_Invoke, this.MenuItem_CommandChanged, this.MenuItem_BeforeQueryStatus, menuCommandID);
                commandService.AddCommand(menuItem);
            }
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static OpenSiblingFilesCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new OpenSiblingFilesCommand(package);
        }

        protected override List<string> GetFileList()
        {
            var folders = new List<string>();
            var files = new List<string>();
			this.SelectedItems.ForEach(item =>
            {
                var folder = Path.GetDirectoryName(item.FileNames[1]);
                if (Directory.Exists(folder) && !folders.Contains(folder))
                {
                    folders.Add(folder);
                }
            });

            folders.ForEach(folder =>
            {
                Directory.GetFiles(folder).ToList().ForEach(file =>
                {
                    files.Add(file);
                });
            });

            return files;
        }
    }
}
