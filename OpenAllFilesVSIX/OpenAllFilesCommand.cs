﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace OpenAllFilesVSIX
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class OpenAllFilesCommand : CommandBase
    {
        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("71dbb498-3661-4a4c-ac48-f79b0f4df874");

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenAllFilesCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private OpenAllFilesCommand(Package package) : base(package)
        {
            this.dte2 = this.ServiceProvider.GetService(typeof(SDTE)) as DTE2;

            var commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new OleMenuCommand(this.MenuItem_Invoke, this.MenuItem_CommandChanged, this.MenuItem_BeforeQueryStatus, menuCommandID);
                commandService.AddCommand(menuItem);
            }
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static OpenAllFilesCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new OpenAllFilesCommand(package);
        }

        protected override List<string> GetFileList()
        {
            var files = new List<string>();
			foreach (var item in this.SelectedItems)
            {
                foreach (var file in Directory.EnumerateFiles(item.FileNames[1], "*.*", SearchOption.AllDirectories))
                {
                    files.Add(file);
                }
            }
            return files;
        }
    }
}
