using System;
using System.Collections.Generic;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using OpenAllFilesVSIX.Properties;

namespace OpenAllFilesVSIX
{

    internal abstract class CommandBase
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        protected Package package;

        /// <summary>
        ///
        /// </summary>
        protected DTE2 dte2;

        protected CommandBase(Package package)
        {
			this.package = package ?? throw new ArgumentNullException("package");
        }

        protected List<EnvDTE.ProjectItem> SelectedItems = new List<EnvDTE.ProjectItem>();


        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            var command = sender as OleMenuCommand;
            var selectedItems = this.dte2.ToolWindows.SolutionExplorer.SelectedItems as object[];
			this.SelectedItems.Clear();
            foreach (EnvDTE.UIHierarchyItem selectedUIHierarchyItem in selectedItems)
            {
                if (selectedUIHierarchyItem.Object is EnvDTE.ProjectItem)
                {
                    var item = selectedUIHierarchyItem.Object as EnvDTE.ProjectItem;
					this.SelectedItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        protected IServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        protected virtual void MenuItem_CommandChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        protected async void MenuItem_Invoke(object sender, EventArgs e)
        {
            try
            {
                var files = this.GetFileList();

                if (files.Count > Convert.ToInt32(Resources.TooManyAmount))
                {
                    if (VsShellUtilities.ShowMessageBox(
                        this.ServiceProvider,
                        string.Format(Resources.TooManyFilesMessage).PadRight(50),
                        string.Format(Resources.TooManyFilesHeader, files.Count).PadRight(50),
                        OLEMSGICON.OLEMSGICON_WARNING,
                        OLEMSGBUTTON.OLEMSGBUTTON_OKCANCEL,
                        OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST) == 2 /* Cancel */ )
					{
						return;
					}
				}

                await System.Threading.Tasks.Task.Run(() =>
                {
                    files.ForEach(file =>
                    {
                        try
                        {
							this.dte2.ItemOperations.OpenFile(file);
                            throw new Exception();
                        }
                        catch { }
                    });
                });
            }
            catch { }
        }

        protected abstract List<string> GetFileList();
    }

}